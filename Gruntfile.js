module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ['dist'],
    copy: {
      main: {
        expand: true,
        cwd: '.',
        src: ['index.html', 'app/**', 'assets/**', '!app/**/*.js', '!assets/styles/**/*.scss'],
        dest: 'dist/'
      }
    },
    jsdoc: {
      dist: {
        src: ['app/**/*.js'],
        options: {
          destination: 'doc'
        }
      }
    },
    jshint: {
      all: ['app/**/*.js', 'assets/js/*.js'],
      gruntfile: ['Gruntfile.js'],
      options: {
        bitwise: true,
        camelcase: true,
        curly: true,
        eqeqeq: true,
        es3: false,
        forin: true,
        freeze: true,
        immed: true,
        indent: 2,
        latedef: 'nofunc',
        newcap: true,
        noarg: true,
        noempty: true,
        nonbsp: true,
        nonew: true,
        plusplus: false,
        quotmark: 'single',
        undef: true,
        unused: false,
        strict: false,
        maxparams: 10,
        maxdepth: 5,
        maxstatements: 40,
        maxcomplexity: 8,
        maxlen: 120,

        asi: false,
        boss: false,
        debug: false,
        eqnull: true,
        esnext: false,
        evil: false,
        expr: false,
        funcscope: false,
        globalstrict: false,
        iterator: false,
        lastsemic: false,
        laxbreak: false,
        laxcomma: false,
        loopfunc: true,
        maxerr: false,
        moz: false,
        multistr: false,
        notypeof: false,
        proto: false,
        scripturl: false,
        shadow: false,
        sub: true,
        supernew: false,
        validthis: false,
        noyield: false,

        browser: true,
        node: true,

        globals: {
          angular: false,
          $: false
        }
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      app: {
        files: {
          'dist/<%= pkg.name %>.js': ['dist/<%= pkg.name %>.js']
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: ['app/**/*.module.js','app/**/*.js'],
        // the location of the resulting JS file
        dest: 'dist/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/<%= pkg.name %>.min.js': ['dist/<%= pkg.name %>.js']
        }
      }
    },
    watch: {
      configFiles: {
        files: [ 'Gruntfile.js'],
        tasks: ['jshint:gruntfile'],
        options: {
          reload: true
        }
      },
      src: {
        files: ['index.html','app/**/*','assets/**/*'],
        tasks: ['jshint', 'clean', 'compass', 'copy', 'concat', 'ngAnnotate', 'uglify'],
        options: {
          livereloadOnError: false,
          livereload: true
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 8080,
          base: 'dist/',
          hostname: 'localhost',
          open: true
        }
      }
    },
    compass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          sassDir: 'assets/styles/',
          cssDir: 'dist/assets/styles/'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-compass');

  //Default task, used fo development
  grunt.registerTask('default', ['jshint', 'clean', 'compass', 'copy', 'concat',
    'ngAnnotate', 'uglify','connect','watch']);
  //Like default but with tests (to add)

  //For tests (to add)
  grunt.registerTask('test', ['jshint']);

};