### Install ###
1. npm install
2. enable the auto refresh of the browser by installing the [LiveReload](http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions-) extension

### Run ###
Dev (default) : 
```
#!script

grunt
```
Tests : 
```
#!script

grunt test
```
Generate docs: 
```
#!script

grunt doc
```

### Libraries ###
Pop up messages like JQuery http://tameraydin.github.io/ngToast/#
Sass for css http://sass-lang.com/guide
I18n http://angular-translate.github.io/
Pluralization https://github.com/SlexAxton/messageformat.js