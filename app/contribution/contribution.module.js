/**
 * Contribution module.
 * Contains every component used to display and manage the contribution and invitations to contribute.
 */
(function() {
  'use strict';

  angular
    .module('app.contribution', []);

})();