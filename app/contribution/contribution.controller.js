(function() {
  'use strict';

  angular
    .module('app.contribution')
    .controller('ContributionController', ContributionController);

  /**
   * Main controller for the Contribution module.
   * @ngInject
   */
  function ContributionController() {

  }

})();