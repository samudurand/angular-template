/**
 * Routes for the Contribution module.
 */
(function() {
  'use strict';

  angular
    .module('app.contribution')
    .config(contRouteConfig);

  /* @ngInject */
  function contRouteConfig($routeProvider) {
    $routeProvider
      .when('/contributions', {
        templateUrl : 'app/contribution/contribution.tpl.html',
        controller: 'ContributionController',
        controllerAs: 'vm'
      });
  }

})();