/**
 * Exceptions handling
 */
(function() {
  'use strict';

  angular
    .module('app.common')
    .config(exceptionConfig);

  /**
   * Exception decorator handling every angular exception uncaught
   */
  function exceptionConfig($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
  }

  /**
   * Handle specific exceptions by writing it in the log and showing a ui message
   */
  function extendExceptionHandler($delegate, ngToast, logger) {
    return function(exception, cause) {
      $delegate(exception, cause);
      logger.logError('An exception occured : ' + exception.msg);
      ngToast.create('Oooops error while processing the last request. Please try again.');
    };
  }

})();
