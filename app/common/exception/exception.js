(function() {
  'use strict';

  angular
  .module('app.common')
  .factory('exception', exception);

  /**
   * Exception catcher. Write any error in the logs.
   */
  function exception(logger) {
    var service = {
      catcher: catcher
    };
    return service;

    function catcher(message) {
      return function(reason) {
        logger.error(message, reason);
      };
    }
  }

})();