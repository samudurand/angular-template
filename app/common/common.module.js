/**
 * Common module. Contains every component shared across the application.
 */
(function() {
  'use strict';

  angular
    .module('app.common', []);

})();
