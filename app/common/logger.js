/**
 * Logger Factory
 */
(function() {
  angular
    .module('app.common')
    .factory('logger', logger);

  /**
   * Application wide logger
   */
  function logger($log) {
    var service = {
      logError: logError
    };
    return service;

    /**
     * Logs errors
     * @param {String} msg Message to log
     * @returns {String} logged message
     */
    function logError(msg) {
      var loggedMsg = 'Error: ' + msg;
      $log.error(loggedMsg);
      return loggedMsg;
    }
  }
})();
