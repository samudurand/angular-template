/**
 * Routes for the Home module.
 */
(function() {
  'use strict';

  angular
    .module('app.home')
    .config(homeRouteConfig);

  /* @ngInject */
  function homeRouteConfig($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl : 'app/home/home.tpl.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      });
  }



})();