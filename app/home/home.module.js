/**
 * Home module.
 * Contains every component used to manage the home page. The home page contains the lists.
 */
(function() {
  'use strict';

  angular
    .module('app.home', []);

})();