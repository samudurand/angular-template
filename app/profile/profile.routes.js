/**
 * Routes for the Profile module.
 */
(function() {
  'use strict';

  angular
    .module('app.profile')
    .config(profileRouteConfig);

  /* @ngInject */
  function profileRouteConfig($routeProvider) {
    $routeProvider
      .when('/profile', {
        templateUrl : 'app/profile/profile.tpl.html',
        controller: 'ProfileController',
        controllerAs: 'vm'
      });
  }



})();