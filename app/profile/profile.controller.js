(function() {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileController', ProfileController);

  /**
   * Main controller for the Profile module.
   * @ngInject
   */
  function ProfileController() {

  }

})();