/**
 * Profile module.
 * Contains every component used to display and manage the user profile and admin settings.
 */
(function() {
  'use strict';

  angular
    .module('app.profile', []);

})();