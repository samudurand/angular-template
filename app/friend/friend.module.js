/**
 * Friend module.
 * Contains every component used to display and manage the friends and friend requests.
 */
(function() {
  'use strict';

  angular
    .module('app.friend', []);

})();