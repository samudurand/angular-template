(function() {
  'use strict';

  angular
    .module('app.friend')
    .controller('FriendController', FriendController);

  /**
   * Main controller for the Friend module.
   * @ngInject
   */
  function FriendController() {

  }

})();