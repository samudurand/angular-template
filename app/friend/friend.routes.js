/**
 * Routes for the Friend module.
 */
(function() {
  'use strict';

  angular
    .module('app.friend')
    .config(friendRouteConfig);

  /* @ngInject */
  function friendRouteConfig($routeProvider) {
    $routeProvider
      .when('/friends', {
        templateUrl : 'app/friend/friend.tpl.html',
        controller: 'FriendController',
        controllerAs: 'vm'
      });
  }



})();