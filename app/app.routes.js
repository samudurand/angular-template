/**
 * Global application routes.
 */
(function() {

  angular
    .module('app')
    .config(routeConfig);

  /* @ngInject */
  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/home'
      });
  }

})();