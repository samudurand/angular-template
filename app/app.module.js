/**
 * Main module.
 */
(function() {
  'use strict';

  angular
    .module('app', [
      'ngRoute','ngToast','ngAnimate','ngSanitize','ngCookies','pascalprecht.translate',
      'app.home','app.friend','app.contribution','app.profile','app.common'
    ])
    .config(handleTranslations)
    .run(handleRoutingErrors);

  /**
   * Handle all routing errors.
   * @ngInject
   */
  function handleRoutingErrors($log, $rootScope, ngToast) {
    $rootScope.$on('$routeChangeError',
      function(event, current, previous, rejection) {
        var destination = (current && (current.title || current.name || current.loadedTemplateUrl)) ||
          'unknown target';
        var msg = 'Error routing to ' + destination + '. ' + (rejection.msg || '');

        ngToast.create(msg);
        $log.error(msg);
      }
    );
  }

  /**
   * Handle all translations.
   * @ngInject
   */
  function handleTranslations($translateProvider) {
    $translateProvider
      .registerAvailableLanguageKeys(['en', 'fr'], {
        'en_US': 'en',
        'en_UK': 'en',
        'fr_FR': 'fr',
        'fr_CA': 'fr'
      })
      .useStaticFilesLoader({
        prefix: 'assets/js/i18n/locale-',
        suffix: '.json'
      })
      .addInterpolation('$translateMessageFormatInterpolation')
      .useLoaderCache(true)
      .determinePreferredLanguage()
      .useLocalStorage()
      .fallbackLanguage(['en','fr'])
      .useMissingTranslationHandlerLog()
      .translations('en', {
        'hello': 'hi {GENDER, select, male{mister !} female{miss !} other{...something?}}'
      });
  }

})();